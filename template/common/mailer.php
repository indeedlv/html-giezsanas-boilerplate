<?php
if($_POST['cop_name']){
    // $to = "info@aatri.lv";
    $to = "info@aatri.lv, arnis@indeed.pro";
    // $to = 'jekabs.kalnins@indeed.pro';
    $subject = "Pieteikšanās forma";
    $form_fields = [ 
        'cop_name' => 'Uzņēmuma nosaukums',
        'reg_num' => 'Uzņēmuma reģistrācijas numurs',
        'darb_noz' => 'Darbības nozare',
        'parst_noz' => 'Pārstāvja nozare',
        'number' => 'Telefona numurs',
        'email' => 'E-pasta adrese',
    ];
    $message = "<h1>Jauns klients vēlas pieteikties servisam</h1><br/>";
    $message .= "Dati:<br/>";

    foreach ($_POST as $key => $info){
        $message .= "$form_fields[$key]: $info <br/>";
    }
    $headers[] = "From: <info@aatri.lv>";
    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/html; charset=UTF-8';

    mail($to,$subject,$message,implode("\r\n", $headers));

    $_POST = array();
    if(isset($_REQUEST["destination"])){
        header("Location: {$_REQUEST["destination"]}");
    }else if(isset($_SERVER["HTTP_REFERER"])){
        header("Location: {$_SERVER["HTTP_REFERER"]}");
    }
}
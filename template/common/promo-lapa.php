<?php
require ('indeed-development.php'); // Vari paskatīties man nenāk šis

?>
<!doctype html>
<html lang="en">
<?php $cache = '?1' ?>
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" type="text/css" href="./../../../aatri/styles/main.css<?php echo $cache ?>" media="screen">
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<div class="container-fluid">
  <header class="row header">
    <div class="col-12 d-flex justify-content-between">
      <div class="logo">
        <img src="./../../../aatri/assets/images/logo.jpg" alt="">
      </div>
      <a href="#" class="btn btn-primary align-self-end header-button"><i class="fas fa-users"></i>Pieteikt sadarbību ar aatri.lv</a>
    </div>
  </header>
  <main id="content">
    <div class="row">
      <div class="col-12 back-image top-banner text-right">
        <div text>
        <h1>AATRI.LV ATROD, PRECI UN PAKALPOJUMI VĒL ĀTRĀK UN IZDEVĪGĀK!</h1>
        <p>Mēs esam AATRI.lv, radoša speciālistu komanda ar vienu mērķi - atvieglot preču un pakalpojumu meklēšanu un salīdzīnāšanu šajā infomrācijas pārpildītajā laikmetā</p>
        </div>
      </div>
    </div>
    <div class="row what-is">
      <div class="col-12 text-center">
        <h2>KAS IR AATRI.LV</h2>
          <p>Baltijas mēroga preču un pakalpojumu meklēšnas un salīdzināšanas platforma, kura atrodas izstrādes un testēšanas stadijā</p>
      </div>
    </div>
    <div class="row info">
      <div class="col-12">
        <div class="row text-center">
          <div class="col-lg-4 col-sm-12">
            <h3><i class="fas fa-check"></i>AATRI.LV MĒRĶIS</h3>
            <p>Kļūt par vadošo preču un pakalpojuma cenu salīdzināšamas platoformu baltijas tirgū. AATRI.lv dos iespēju vienuviet iegūt visu oficiālo informāciju un veikt salīdzinājumu pēc cenas un kvalitātes.</p>
          </div>
          <div class="col-lg-4 col-sm-12">
            <h3><i class="fas fa-question"></i>KĀPĒC AATRI.LV?</h3>
            <p>šī ir prefesionāli veidota interneta platforma, kuru izstrādādā un administrē neozaru speciālisti. Veidota, balstoties uz vairāku gadu gūto pieredzi pie nozīmīgiem projektiem valsts un starptautiskā mērogā.</p>
          </div>
          <div class="col-lg-4 col-sm-12">
            <h3><i class="fas fa-plus"></i>KĀ KĻŪT PAR AATRI.LV PARTNERI?</h3>
            <p>Pietekties var - aizpildot pieteikšanās formu vai sūtot veikala informāciju par jūsu i-veikalu uz <a href="mailto:info@aatri.lv">info@aatri.lv</a> un mūsu klientu speciālists ar Jums sazināsies lai varētu vienoties par tikšanos klātbūtnē un atbildēs uz viesiem Jums interesējošajiem jautājumiem.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row form back-image">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-lg-6 col-sm-8">
            <form action="./mailer.php" method="post">
              <h3>PIETEIKŠANĀS FORMA</h3>
              <p>Lai pieteiktos, lūdzu aizpildiet formu</p>
              <input type="text" name="cop_name" placeholder="Uzņēmuma nosaukums*">
              <input type="text" name="reg_num" placeholder="Uzņēmuma reģistrācijas numurs*">
              <input type="text" name="darb_noz" placeholder="Darbības nozare*">
              <input type="text" name="parst_noz" placeholder="Pārstāvja nozare*">
              <input type="text" name="number" placeholder="Telefona numurs*">
              <input type="text" name="email" placeholder="E-pasta adrese*">
              <input type="submit" value="PIETEIKTIES" class="btn">
            </form>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>
  <footer>
    <p class="text-center">Visas tiesības aizsargātas &copy; AATRI.LV, 2018</p>
  </footer>
</body>
</html>

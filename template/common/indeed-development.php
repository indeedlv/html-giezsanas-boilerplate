<?php
/*
  Development functions, created by arnis.juraga@gmail.com @ INDEED, Ltd / Copona.org
  works from specific IP addresses.

  To work, must be included in /system/startup.php after
  require_once(DIR_SYSTEM . 'helper/utf8.php');
  by
  require_once(DIR_SYSTEM . 'helper/indeed-development.php');
  pr($data, $args)
 * if args('raw') - will not ouput HTML tags.
 *
 *
 */

/* * ************************ CONFIG ***************** */
$ip = !empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

if ($ip == '80.232.223.246' OR
    $ip == '109.110.19.154' OR
    $ip == '62.84.24.156' OR
    $ip == '87.110.183.114' OR
    $ip == '109.110.25.253' OR
    $ip == '213.226.141.130' OR // jekabs

    strpos($ip, "192.168.1.") === 0 OR
    strpos($ip, "127.0.0.") === 0 OR
    strpos($ip, "10.1.1.") === 0 OR
    strpos($ip, "5.179.9.") === 0 OR // LU telpas eduroam

  strpos($ip, "192.168.0.") === 0) {
    $debug_ip = TRUE;
} else {
    $debug_ip = FALSE;
}
/* * *********************************************** */

// debug hack start
/*
  if($_SERVER['REMOTE_ADDR']=='80.232.223.246' OR $_SERVER['REMOTE_ADDR']=='213.175.120.130') {
  define('DEBUG', true); // comment to disable debug mode
  define('SQL_DEBUG', true); // comment to disable sql-debugging
  }
  else
  {
  } // */

/*
  // DISPLAY ALL ERRORS VIENM�R. Bet jâliek startup.php fail�
  ini_set('display_errors', 1);
  error_reporting(E_ALL ^ E_NOTICE);
 */



if (defined('DEBUG')) {
    $start_time = microtime();
    $start_mem = memory_get_usage();
}
// debug hack endd

if (!function_exists('pr')) {

    function pr($data = 'MAINĪGAIS NAV PADOTS', $arg = 'default') {
        global $debug_ip;
        echo "\n\n";

        if ($debug_ip) {

            if ($arg!='raw') {
                echo "<div style='border: 1px solid grey; padding: 5px;'>";
                echo "<span style='color: black; background-color: white; font-size: 12px;'>\nPR data: <strong>" . gettype($data) . "</strong></span>\n";
                echo "<pre style='background-color: #EACCCC; white-space: pre-wrap; font-size: 14px; color: red; padding: 10px; margin: 0; line-height: 14px;'>\n";
            }

            if ($data === '')
                echo "EMPTY STRING\n";
            elseif ($data === ' ')
                echo "TUKŠUMS\n";
            elseif ($data === 0)
                echo " 0 \n";
            elseif ($data === FALSE)
                echo "FALSE \n";
            elseif ($data === NULL)
                echo "UNDEFINED\n";
            elseif (gettype($data) == 'string') {
                print_r(htmlentities($data));
            } else
                print_r($data);
            echo "\n</pre>\n";


            $debug = debug_backtrace();

            $file_from = file($debug[0]['file']);

            if ($arg!='raw') {
                foreach ($debug as $file) {
                    echo "<span style='font-size: 12px;'>\n<strong>" . trim($file_from[$debug[0]['line'] - 1]) . "</strong>\n</span><br />\n";
                    echo "<span style='font-size: 12px;'>" . $file['file'] . "</span>:\n";
                    echo "<span style='font-size: 12px; color: red; font-weight: bold;'>" . $file['line'] . "</span> <br />\n";
                    break;
                }
                echo "</div>";
            } else {
                echo trim($file_from[$debug[0]['line'] - 1]). "\n";
                    echo $file['file']. "\n";
                    echo $file['line']. "\n";

            }
        }
    }

}

if (!function_exists('prd')) {

    function prd($data = 'MAINĪGAIS NAV PADOTS') {
        global $debug_ip;
        echo "\n\n";

        //$ip = $_SERVER['REMOTE_ADDR'];

        if ($debug_ip) {

            echo "<div style='border: 1px solid grey; padding: 5px;'>";
            echo "<span style='color: black; background-color: white; font-size: 12px;'>\nPRD data: <strong>" . gettype($data) . "</strong></span>\n";
            echo "<pre style='white-space: pre-wrap; background-color: #ccc; padding: 10px;  font-size: 14px; color: black; margin: 0; line-height: 14px;'>\n";

            if ($data === '')
                echo "empty STRING\n";
            elseif ($data === ' ')
                echo "empty SPACE\n";
            elseif ($data === 0)
                echo " 0 \n";
            elseif ($data === FALSE)
                echo "FALSE \n";
            elseif ($data === NULL)
                echo "UNDEFINED\n";
            elseif (gettype($data) == 'string') {
                print_r(htmlentities($data));
            } else {
                print_r($data);
            }
            echo "\n</pre>\n";

            $debug = debug_backtrace();
            $file_from = file($debug[0]['file']);

            foreach ($debug as $file) {
                echo "<span style='font-size: 12px;'>\n<strong>" . trim($file_from[$debug[0]['line'] - 1]) . "</strong>\n</span><br />\n";
                echo "<span style='font-size: 12px;'>" . $file['file'] . "</span>:\n";
                echo "<span style='font-size: 12px; color: red; font-weight: bold;'>" . $file['line'] . "</span> <br />\n";
                break;
            }
            echo "</div>";
            die();
        }
    }

}

// Vajadzīgais stafs
var gulp = require('gulp'),
    env = require('./_env.json'), // ielasām environment mainīgos.
    gutil = require('gulp-util'),
    buffer = require('vinyl-buffer'), // spritessmith
    imagemin = require('gulp-imagemin'), // spritesmith
    csso = require('gulp-csso'), // spritesmith
    merge = require('merge-stream'), // spritesmith
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    cached = require('gulp-cached'),
    spritesmith = require('gulp.spritesmith'), // !!! vajag tieši ar PUNKTU !!!
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean');

// Tēmas direktorija, 'nemam no _env.json faila (kā paraugs)
var templateDirName = env.theme_folder;

// CSS
var cssDirName = 'styles',
    imagesSrcDirName = 'images',
    cssName = 'main.css'; // Rozesbode ir main.css, citiem cits.

// JS direktorija, kur atrodas stili un js faila nosaukums
var jsName = 'main.js'; // js Destination fails
var jsNamePlugins = 'plugins.js'; // js Destination fails


// Faili, failu ceļi
var paths = {
    script_plugins: ['js/jquery.fancybox.js', 'js/modernizr.js', 'bower_components/slick-carousel/slick/slick.js', 'js/jquery.placeholder.js'],
    script_main: ['scripts/**/*.js'],
    script_common: ['assets/js/*.js'],
    styles: ['scss/**/*.scss'],
    cssDir: '../' + templateDirName + '/' +  cssDirName + '/',
    jsDir: 'scripts', // js Destination direktorija
};


// Browsersync main'igie, configam
var browsersync = require('browser-sync');

bconfig = {
    //files: [build+'/**', '!'+build+'/**.map'], // Exclude map files
    files: ['../' + templateDirName + '/**', !'../' + templateDirName + '/**.map'], // Exclude map files
    notify: false, // In-line notifications (the blocks of text saying whether you are connected to the BrowserSync server or not)
    open: true, // Set to false if you don't like the browser window opening automatically
    port: 3000, // Port number for the live version of the site; default: 3000
   proxy: (env.proxy || 'http://localhost'), // We need to use a proxy instead of the built-in server because WordPress has to do some server-side rendering for the theme to work
    watchOptions: {
        debounceDelay: 100, // This introduces a small delay when watching for file change events to avoid triggering too many reloads
        reloadDelay: 100
    }
}

// BrowserSync: be sure to setup `proxy` in `/gulpconfig.js`
// Quick start: connect all your devices to the same network (e.g. wifi) and navigate to the address output in the console when you run `gulp`
gulp.task('browsersync', ['build'], function () {
    gutil.log('Palaidies browsersync!')
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.script_main, ['scripts']);
    gulp.watch(paths.script_common, ['scripts_common']);
    gulp.watch('template/**/*.*', ['templates']);
    gulp.watch('language/**/*.*', ['templates']);
    gulp.watch('src/sprites/*.png', ['sprite', 'styles']);

    browsersync(bconfig);
});


gulp.task('styles', function () {
    gulp.src(paths.styles)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cached('styles'))
        .pipe(sourcemaps.write())
        .pipe(rename(cssName))
        .pipe(gulp.dest(paths.cssDir))
});


// KOPĒJAM TEMPLEITUS
gulp.task('templates', function () {
  gulp.src('template/functions.php') // Kopējam functyions
    .pipe(cached('functions'))
    .pipe(gulp.dest('../' + templateDirName + '/'));
    gulp.src('template/**/*.*') // Kopējam templeitus
      .pipe(cached('template'))
        .pipe(gulp.dest('../' + templateDirName + '/template'));
    gulp.src('language/**/**') // Kopējam tēmmas valodas tulkojumus
        .pipe(cached('language'))
        .pipe(gulp.dest('../' + templateDirName + '/language/'))
});

//Tupa kopējam visu, ko vajag pārkopēt uz dest
gulp.task('copy', function () {
    gulp.src('images/**/**') // Kopējam Bildes
      .pipe(cached('images'))
        .pipe(gulp.dest('../' + templateDirName + '/assets/images'))
    gulp.src('fonts/**/**') // Kopējam Fontus
      .pipe(cached('fonts'))
        .pipe(gulp.dest('../' + templateDirName + '/assets/fonts'))
    gulp.src('assets/**/**')             // Kopējam Skriptus
      .pipe(cached('assets'))
        .pipe(gulp.dest('../' + templateDirName + '/assets/'))
});


// Skripti
gulp.task('scripts', function () {
    // gulp.src('scripts/**/*.js')
    gulp.src(paths.script_plugins)
      .pipe(cached('scripts'))
        .pipe(concat(jsNamePlugins))
        .pipe(uglify())
        //  .pipe(rename(jsName)) // šis pārsauc failu tā, kā mums vajag
        .pipe(gulp.dest('../' + templateDirName + '/' + paths.jsDir + '/'))
    gulp.src(paths.script_main)
        .pipe(concat(jsName))
        // .pipe(uglify())
        //  .pipe(rename(jsName)) // šis pārsauc failu tā, kā mums vajag
        .pipe(gulp.dest('../' + templateDirName + '/' + paths.jsDir + '/'))
});
gulp.task('scripts_common', function () {
  gulp.src(paths.script_common)
    .pipe(gulp.dest('../' + templateDirName + '/assets/js'))
});




//Watch task

// gulp.task('watch', function () {
//     gutil.log('Palaidies watch!');
//     gulp.watch(paths.script_main, ['scripts']);
//     gulp.watch(paths.script_common, ['scripts_common']);
//     gulp.watch('template/**/*.*', ['templates']);
//     gulp.watch('language/**/*.*', ['templates']);
//     gulp.watch(paths.styles, ['styles']);
//     gulp.watch(imagesSrcDirName + '/**/*.*', ['sprite']); // kkas nestrādā tāpat
// });

// Sprites task
gulp.task('sprite', function () {
    gutil.log('Palaidies sprites!')
    // Generate our spritesheet
    var spriteData = gulp.src('src/sprites/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css',
        cssOpts: {
            cssSelector: function (item) {
                // If this is a hover sprite, name it as a hover one (e.g. 'home-hover' -> 'home:hover')
                if (item.name.indexOf('-hover') !== -1) {
                    return '.icon-' + item.name.replace('-hover', ':hover');
                    // Otherwise, use the name as the selector (e.g. 'home' -> 'home')
                }
                else {
                    return '.icon-' + item.name;
                }
            }
        }
    }));
    // Pipe image stream through image optimizer and onto disk
    var imgStream = spriteData.img
    // DEV: We must buffer our stream into a Buffer for `imagemin`
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(gulp.dest(paths.cssDir)); // jābūt iekš Styles direktorijas, jo ceļi relatīvi.
    // Pipe CSS stream through CSS optimizer and onto disk
    var cssStream = spriteData.css
    // .pipe(csso())    <<---- minimize CSS
        .pipe(gulp.dest('src/css/')); // liekam iekš SRC pagaidām
    // Return a merged stream to handle both `end` events
    return merge(imgStream, cssStream);
});

// Build task

gulp.task('build', ['sprite', 'templates', 'copy', 'styles', 'scripts', 'scripts_common']);
